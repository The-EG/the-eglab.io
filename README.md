This blog is now hosted on GitHub: [https://github.com/The-EG/The-EG.github.io](https://github.com/The-EG/The-EG.github.io)

~~This is the source for my blog, [Musings of an evil genius](https://the-eg.gitlab.io).~~

~~It is written in markdown, with the content written by [Jekyll](https://jekyllrb.com/) using the minima theme.~~