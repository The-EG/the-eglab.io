---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: home
---
Moved to GitHub!

[https://the-eg.github.io/](https://the-eg.github.io/)